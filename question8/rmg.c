#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/stat.h>
#include <unistd.h>
#include <assert.h>
#include "check_pass.h"

#define FILENAME_PWD "/home/admin/passwd"

/**
 * Récupére le groupe id d'un fichier
 * @param filename Emplacement du fichier
 * @return
 */
int getgid_file(char *filename) {

    struct stat stats;

    if (stat(filename, &stats) == 0) {
        return stats.st_gid;
    } else {
        perror("Cannot stat file");
        exit(EXIT_FAILURE);
    }
}

/**
 * Récupére le uid de l'utilisateur osus forme de string
 * @return String avec le uid
 */
char *uidToStr() {
    uid_t uid;
    uid = getuid();

    char *uidstr = malloc(sizeof(char) * 10);
    sprintf(uidstr, "%d", uid);

    return uidstr;
}

int main(int argc, char *argv[]) {
    if (argc < 2) {
        perror("Missing argument\n");
        exit(EXIT_FAILURE);
    }

    FILE *fpPwd;
    char *uidString;
    char *filename = argv[1];

    // Check user belongs to same group as file
    int rgid_user = getgid();
    int rgid_file = getgid_file(filename);

    if (rgid_user != rgid_file) {
        perror("User group and file group are not the same !");
        exit(EXIT_FAILURE);
    }

    fpPwd = fopen(FILENAME_PWD, "r");
    if (!fpPwd) {
        perror("Unable to open the password file\n");
        exit(EXIT_FAILURE);
    }

    uidString = uidToStr();
    char *registeredPwd = getPassword(fpPwd, uidString);
    if(registeredPwd == NULL){
        perror("You don't have a password in the password file");
        exit(EXIT_FAILURE);
    }

    char *password = getpass("Password:");

    if (strcmp(registeredPwd, password)) {
        if (remove(filename)) {
            printf("The file has been deleted");
        } else {
            perror("Unable to open the file\n");
        }
    }

    fclose(fpPwd);
    exit(EXIT_FAILURE);
}