#ifndef ISI_TP1_DROITS_CHECK_PASS_H
#define ISI_TP1_DROITS_CHECK_PASS_H

#include <stdio.h>
#include "string.h"

#define SEPARATOR ':'
#define SIZE_PWD 128

/**
 * Ajoute un compte au fichier
 * @param fp Pointeur vers le fichier
 * @param user Clef d'identification de l'utilisateur
 * @param password Mot de passe
 * @return Retourne le nombre de bits écrit dans le fichier, la valeur est négative si cela échoue
 */
int addAccount(FILE *fp, const char *user, const char *password);

/**
 * Récupère un mot de passe dans le fichier en cherchant la clef de l'utilisateur
 * @param fp Pointeur vers le fichier
 * @param user Clef d'identification de l'utilisateur
 * @return NULL si la clef n'est pas trouvé, le mot de passe sinon
 */
char *getPassword(FILE *fp, const char *user);

#endif //ISI_TP1_DROITS_CHECK_PASS_H
