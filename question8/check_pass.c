#include "check_pass.h"
#include "string.h"

int addAccount(FILE *fp, const char *user, const char *password) {
    return fprintf(fp, "%s%c%s\n", user, SEPARATOR, password);
}

char *getPassword(FILE *fp, const char *user) {
    char line[SIZE_PWD];
    int found = 0;

    while (found == 0 && fgets(line, SIZE_PWD, fp) != NULL) {
        found = startsWith(user, line);
    }

    if (found) {
        int indexSplit = indexOf(line, SEPARATOR);
        if (indexSplit != -1) {
            return substr(line, indexSplit + 1, strlen(line) - 1);
        }
    }

    return NULL;
}