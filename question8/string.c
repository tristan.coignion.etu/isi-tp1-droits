#include "string.h"

int indexOf(const char *str, char value) {
    int i = 0;
    while (str[i] != value && str[i] != '\0') {
        i++;
    }
    return str[i] == value ? i : -1;
}

char *substr(const char *src, int m, int n) {
    int len = n - m;
    if(len < 0){
        return NULL;
    }

    char *dest = (char *) malloc(sizeof(char) * (len + 1));

    for (int i = m; i < n && (*(src + i) != '\0'); i++) {
        *dest = *(src + i);
        dest++;
    }

    *dest = '\0';

    return dest - len;
}

int startsWith(const char *prefix, const char *source) {
    size_t lenpre = strlen(prefix), lenstr = strlen(source);
    return lenstr < lenpre ? 0 : memcmp(prefix, source, lenpre) == 0;
}