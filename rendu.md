# Rendu "Les droits d’accès dans les systèmes UNIX"

## Binome

- Hanson, Anthony, anthony.hanson@outlook.fr

- Coignion, Tristan, tristan.coignion.etu@univ-lille.fr

## Question 1

_Pour cet exercice et les suivants, créez un utilisateur toto dans le système, et ajoutez-le au groupe ubuntu._

### Réponse

Commandes :
```shell
$ > adduser toto
$ > adduser toto ubuntu
```

_Supposons qu’un processus ait été lancé par l’utilisateur toto. Le processus essaie d’ouvrir en écriture le fichier myfile.txt suivant :
-r--rw-r-- 1 toto ubuntu 5 Jan 5 09:35 titi.txt
Dire si le processus peut écrire, et pourquoi._

### Réponse

Oui toto peut écrire puisqu’il appartient au groupe “ubuntu” qui lui même a le droit en lecture et écriture

## Question 2

_Le caractère x indique que le fichier est exécutable.
Que signifie le caractère x pour un répertoire ?_

### Réponse

Le caractère x sur un répertoire signifie qu’il y a possibilité de visiter l’arborescence du répertoire.

_Avec l’utilisateur ubuntu créez le répertoire mydir, et enlevez le droit d’exécution au groupe ubuntu. Maintenant, avec l’utilisateur toto, essayez d’entrer dans le répertoire avec cd mydir._

### Réponse

Commandes:
```shell
#Créer le répertoire :
ubuntu:$ mkdir mydir
#enlevez le droit d’exécution au groupe ubuntu :
ubuntu:$ chmod g=rw mydir/
```
| argument | Description                             |
| -------- | --------------------------------------- |
| u        | The file owner.                         |
| g        | The users who are members of the group. |
| o        | All other users.                        |
| a        | All users, identical to ugo.            |

_Que se passe-t-il ? Pourquoi ?_

### Réponse

```shell
toto:$ bash: cd: mydir/: Permission denied
```
L’utilisateur toto ne peut plus rentrer dans le fichier. 
En effet avec ls -l on s’aperçoit que l’utilisateur ubuntu possède tous les droits, mais que le groupe ubuntu possède les droits suivants : rw-. 
De ce fait puisque toto appartient au groupe ubuntu, et que l’ordre des vérifications des permissions se font dans l’ordre suivant : user > groupe > all, puisqu’il est perçu comme étant dans le groupe ubuntu, il n’obtient pas les droits d'exécution donnés à all

_Avec l’utilisateur ubuntu, créez un fichier data.txt dans le répertoire mydir. Maintenant, avec l'utilisateur toto essayez de lister le contenu du répertoire avec ls -al mydir.
Que se passe-t-il ? Pourquoi ?_

### Réponse

```shell
toto:$ ls: cannot access 'mydir/.': Permission denied
toto:$ ls: cannot access 'mydir/..': Permission denied
toto:$ ls: cannot access 'mydir/data.txt': Permission denied
toto:$ total 0
toto:$ d????????? ? ? ? ?            ? .
toto:$ d????????? ? ? ? ?            ? ..
toto:$ -????????? ? ? ? ?            ? data.txt
```

Comme dit précédemment, le droit d'exécution sur un répertoire permet de le naviguer. 
Ainsi puisque toto ne possède pas ce droit, il ne peut pas connaître le contenu du répertoire mydir. 
C’est pour cela que les données sont sous forme de “?”


## Question 3

_Écrire un programme en C qui imprime la valeur des ses ids (EUID, EGID, RUID, RGID) et le contenu du fichier mydir/mydata.txt (que vous avez créé à la question 2).
Le fichier exécutable doit appartenir à l’utilisateur ubuntu et au groupe ubuntu. Lancez ce programme avec l’utilisateur toto._

_Quelles sont les valeurs des différents ids ? Le processus arrive-t-il à ouvrir le fichier mydir/mydata.txt en lecture ?_

### Réponse

[Cliquez ici pour voir le Script](./question3/)

Commande pour envoyer le fichier “suid” vers la machine de l’univ
```shell
$ scp -i ~/.ssh/openstack-univ.pem suid ubuntu@172.28.100.251:/home/ubuntu/mydir/suid
```

Lorsqu’on exécute avec toto, nous obtenons le résultat :
Les différents ids sont :

| ID       | Numéro |
| -------- | ------ |
| EUID     | 1001   |
| EGID     | 1001   |
| RUID     | 1001   |
| RGID     | 1001   |

Cependant, le fichier ne peut pas être ouvert, nous avons alors l’erreur : 

Cannot open file: Permission denied

Après avoir effectué la commande : 
```shell
ubuntu:$ chmod u+s suid
toto:$ ./suid mydir/mydata.txt
```
| ID       | Numéro              |
| -------- | ------------------- |
| EUID     | 1000 (id de ubuntu) |
| EGID     | 1001                |
| RUID     | 1001                |
| RGID     | 1001                |

## Question 4

_Écrivez un script python qui imprime les valeurs des EUID et EGID. 
Le script doit appartenir à l’utilisateur ubuntu. 
Activez le set-user-id et lancez le script avec l’utilisateur toto.
Quelles sont les valeurs des différents ids ?_

### Réponse

[Cliquez ici pour voir le Script](./question4/)

```shell
toto:$ python3 suid.py mydir/mydata.txt
```
| ID       | Numéro              |
| -------- | ------------------- |
| EUID     | 1001                |
| EGID     | 1001                |
| RUID     | 1001                |
| RGID     | 1001                |

```shell
ubuntu:$ chmod u+s suid.py
toto:$ python3 suid.py mydir/mydata.txt
```
| ID       | Numéro              |
| -------- | ------------------- |
| EUID     | 1001                |
| EGID     | 1001                |
| RUID     | 1001                |
| RGID     | 1001                |

Comme on peut voir, avec ou sans chmod u+s, on obtient les mêmes ids. 
La raison est que chmod s’applique sur le programme printid.py, cependant, en C cela fonctionne correctement puisque c’est le programme printid qui est lancé. 
En Python cependant, il s’agit du programme python3 qui est lancé (ainsi ne possède pas le changement d’id) et qui exécute le scrypt printid.py.

_Comment un utilisateur peut changer un de ses attributs sans demander à l’administrateur ?_

TODO

## Question 5

_Visualisez le contenu du fichier/etc/passwd.— À quoi sert la commande chfn? Donnez les résultats de ls -al /usr/bin/chfn, et expliquez les permissions._

### Réponse

La commande “chfn” permet de modifier ses informations personnelles, ou celles d’un autre utilisateur.

Commandes :
```shell
$ ls -al /usr/bin/chfn
#Résultat
$ -rwsr-xr-x 1 root root ...
```

“/usr/bin/chfn” est le fichier de la commande associée. 
On remarque que le root peut lire, écrire, et aussi exécuter le fichier “chfn”. 
Le groupe root lui peut exécuter et lire mais pas écrire dans ce fichier. 
Il paraît logique que seul le root puisse modifier l’effet de chfn. 
Après modification du numéro de tél de l’utilisateur toto le fichier a bien été modifié.


## Question 6

_Vous avez pu observer que le fichier/etc/passwdne contient aucun mot de passe.— Où sont stockés les mots de passe des utilisateurs ? Pourquoi ?_

### Réponse

Les mots de passe sont stockés dans /etc/shadow. [Quora](https://www.quora.com/Why-is-the-Linux-password-file-called-shadow)

Il existe deux fichiers, passwd et shadow. Passwd est le fichier de mot de passe Unix original. 
Il y avait un problème de sécurité. 

Tout le monde pouvait le lire et la fonction de cryptage était exceptionnellement faible.
Elle devait être lisible car c'était la seule liste définitive des noms d'utilisateur, des vrais noms et des rôles. 
Ce n'était pas seulement un fichier de mots de passe, c'était une base de données d'utilisateurs complète.

Ainsi, quelque part plus tard, la suite de mots de passe shadow a été développée. Cela a masqué le fichier de mots de passe, d'où le nom. 
Cela contenait les vrais mots de passe, cryptés à l'aide d'algorithmes beaucoup plus puissants. 

Le fichier passwd ne contenait plus du tout de mots de passe.

Le fichier de mot de passe shadow a ensuite été restreint, car seuls les services de mot de passe doivent y accéder. 

Le fichier passwd n'a pas été modifié au-delà de ne pas avoir de mots de passe, pour la compatibilité ascendante et pour prendre en charge Unixen qui n'utilise pas cette approche.
 

## Question 7

Mettre les scripts bash dans le repertoire *question7*.

## Question 8

Le programme et les scripts dans le repertoire *question8*.

## Question 9

Le programme et les scripts dans le repertoire *question9*.

## Question 10

Les programmes *groupe_server* et *groupe_client* dans le repertoire
*question10* ainsi que les tests. 
