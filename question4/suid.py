import sys, os

if __name__ == '__main__':
    assert len(sys.argv) >= 2
    print("EGID =", os.getegid())
    print("EUID =", os.geteuid())
    print("RGID =", os.getgid())
    print("RUID =", os.getuid())
    with open(sys.argv[1], mode="r") as f:
        print(f.readlines())
