#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <unistd.h>

void printstats();

int main(int argc, char *argv[]){
    FILE *f;
    struct stat stats;

    if (argc < 2) {
        printf("Missing argument\n");
        exit(EXIT_FAILURE);
    }

    printstats();

    f = fopen(argv[1], "r");
    if (f == NULL) {
        perror("Cannot open file");
        exit(EXIT_FAILURE);
    }

    if(stat(argv[1], &stats) != 0){
        perror("Unable to get file properties");
        exit(EXIT_FAILURE);
    }

    fclose(f);
    exit(EXIT_SUCCESS);
}

void printstats() {
    printf("EUID = %u\n", geteuid());
    printf("EGID = %u\n", getegid());
    printf("RUID = %u\n", getuid());
    printf("RGID = %u\n", getgid());
}
