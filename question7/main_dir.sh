#!/bin/bash

rm -rdf dir_a
rm -rdf dir_b
rm -rdf dir_c


mkdir dir_a
chown :groupe_a dir_a
chown admin_1 dir_a
chmod g=rwx dir_a
chmod o-rwx dir_a
chmod +t -R dir_a

mkdir dir_b
chown :groupe_b dir_b
chown admin_1 dir_b
chmod g=rwx dir_b
chmod o-rwx dir_b
chmod +t dir_b

mkdir dir_c
chown admin_1 dir_c
chmod o=rx dir_c
chmod +t dir_c

su lambda_a -c 'echo "My file a1" > ./dir_a/file_1_lambda_a'
su lambda_a_2 -c 'echo "My file a2" > ./dir_a/file_1_lambda_a_2'

su lambda_b -c 'echo "My file b2" > ./dir_b/file_1_lambda_b'
su lambda_b_2 -c 'echo "My file b2" > ./dir_b/file_1_lambda_b_2'

su admin_1 -c 'echo "My file admin" > ./dir_c/file_1_admin_1'
