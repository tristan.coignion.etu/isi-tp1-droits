# get_permissions() {
#     y=$1
#     echo ${y:0:10}
# }

# get_name() {
#     y=$1
#     echo ${y:10}
# }

# IFS='
# '
# for x in `ls -l $1`; do echo $(get_permissions $x) $(get_name $x); done

# Can enter dir_a
if [[ -d dir_a ]]; then 
    if [ -x dir_a ] && [ -r dir_a ]; then
        echo "Read and access dir_a : OK"

        # Can read files and subdirectories in dir_a
        if [ -r "dir_a/file_1_lambda_a" ]; then
            echo "Read files an subdirectories in dir_a : OK"
        else
            echo "Read files and subdirectories in dir_a : ERROR"
        fi
    else
        echo "Read and access dir_a : ERROR"
    fi
else 
    echo "dir_a is not a directory" 
fi

#Can create new files and directories
if [ -w "dir_a" ]; then
    echo "Creating new files an subdirectories in dir_a : OK"
else
    echo "Creating new files an subdirectories in dir_a : ERROR"
fi 

# Can modify files in dir_a
if [ -w "dir_a/file_1_lambda_a" ]; then
    echo "Modify files an subdirectories in dir_a : OK"
else
    echo "Modify files and subdirectories in dir_a : ERROR"
fi 

# Cannot delete or rename files in dir_a that don't belong to me
if [ -k "dir_a" ]; then
    echo "Cannot delete or rename files in dir_a that don't belong to me : OK"
else
    echo "Cannot delete or rename files in dir_a that don't belong to me : ERROR"
fi

# Can enter dir_c
if [[ -d dir_c ]]; then 
    if [ -x dir_c ] && [ -r dir_c ]; then
        echo "Read and access dir_c : OK"

        # Can read files and subdirectories in dir_c
        if [ -r "dir_c/file_1_admin_1" ]; then
            echo "Read files an subdirectories in dir_c : OK"
        else
            echo "Read files and subdirectories in dir_c : ERROR"
        fi
    else
        echo "Read and access dir_c : ERROR"
    fi
else 
    echo "dir_c is not a directory" 
fi

# Cannot write in dir_c ni créer des nouveaux fichiers.
if ! [ -w dir_c ]; then
    echo "Cannot create new files in dir_c : OK"
else
    echo "Cannot create new files in dir_c  : ERROR"
fi

# Cannot write in dir_c ni créer des nouveaux fichiers.
if ! [ -w "dir_c/file_1_admin_1" ]; then
    echo "Cannot modify files in dir_c : OK"
else
    echo "Cannot modify files in dir_c  : ERROR"
fi

# Cannot rename or remove files in dir_c
if [ -k "dir_c" ]; then
    echo "Cannot rename or delete files in dir_c : OK"
else
    echo "Cannot rename or delete files in dir_c : ERROR"
fi

if [[ -d dir_b ]]; then
    if ! [ -r dir_b ] && ! [ -w dir_b ] && ! [ -x dir_b ]; then
        echo "Cannot read, write or acces files in dir_b : OK"
    else
        echo "Cannot read, write or acces files in dir_b : ERROR"
    fi
else
    echo "dir_b is not a directory"
fi

if [ -k "dir_b" ]; then
    echo "Cannot rename or delete files in dir_b : OK"
else
    echo "Cannot rename or delete files in dir_b : ERROR"
fi