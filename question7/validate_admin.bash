# Can delete rename everywhere

# Can modify read and execute everywhere

# get_permissions() {
#     y=$1
#     echo ${y:0:10}
# }

# get_name() {
#     y=$1
#     echo ${y:10}
# }

# IFS='
# '
# for x in `ls -l $1`; do echo $(get_permissions $x) $(get_name $x); done

# Can enter dir_a
if [[ -d dir_a ]]; then 
    if [ -x dir_a ] && [ -r dir_a ]; then
        echo "Read and access dir_a : OK"

        # Can read files and subdirectories in dir_a
        if [ -r "dir_a/file_1_lambda_a" ]; then
            echo "Read files an subdirectories in dir_a : OK"
        else
            echo "Read files and subdirectories in dir_a : ERROR"
        fi
    else
        echo "Read and access dir_a : ERROR"
    fi
else 
    echo "dir_a is not a directory" 
fi

#Can create new files and directories
if [ -w "dir_a" ]; then
    echo "Creating new files an subdirectories in dir_a : OK"
else
    echo "Creating new files an subdirectories in dir_a : ERROR"
fi 

if [ -O "dir_a" ]; then
    echo "Can delete or rename files in dir_a that don't belong to me : OK"
else
    echo "Can delete or rename files in dir_a that don't belong to me : ERROR"
fi


# Can enter dir_b
if [[ -d dir_b ]]; then 
    if [ -x dir_b ] && [ -r dir_b ]; then
        echo "Read and access dir_b : OK"

        # Can read files and subdirectories in dir_b
        if [ -r "dir_b/file_1_lambda_b" ]; then
            echo "Read files an subdirectories in dir_b : OK"
        else
            echo "Read files and subdirectories in dir_b : ERROR"
        fi
    else
        echo "Read and access dir_b : ERROR"
    fi
else 
    echo "dir_b is not a directory" 
fi

#Can create new files and directories
if [ -w "dir_b" ]; then
    echo "Creating new files an subdirectories in dir_b : OK"
else
    echo "Creating new files an subdirectories in dir_b : ERROR"
fi 

# Can delete or rename files in dir_b that don't belong to me
if [ -O "dir_b" ]; then
    echo "Cannot delete or rename files in dir_b that don't belong to me : OK"
else
    echo "Cannot delete or rename files in dir_b that don't belong to me : ERROR"
fi

# Can enter dir_c
if [[ -d dir_c ]]; then 
    if [ -x dir_c ] && [ -r dir_c ]; then
        echo "Read and access dir_c : OK"

        # Can read files and subdirectories in dir_c
        if [ -r "dir_c/file_1_admin_1" ]; then
            echo "Read files an subdirectories in dir_c : OK"
        else
            echo "Read files and subdirectories in dir_c : ERROR"
        fi
    else
        echo "Read and access dir_c : ERROR"
    fi
else 
    echo "dir_c is not a directory" 
fi


if [ -w dir_c ]; then
    echo "Can create new files in dir_c : OK"
else
    echo "Can create new files in dir_c  : ERROR"
fi


if [ -w "dir_c/file_1_admin_1" ]; then
    echo "Can modify files in dir_c : OK"
else
    echo "Can modify files in dir_c  : ERROR"
fi

if [ -O "dir_c" ]; then
    echo "Can rename or delete files in dir_c : OK"
else
    echo "Can rename or delete files in dir_c : ERROR"
fi