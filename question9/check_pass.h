#ifndef ISI_TP1_DROITS_CHECK_PASS_H
#define ISI_TP1_DROITS_CHECK_PASS_H

#include <stdio.h>
#include "string.h"
#include <crypt.h>
#include <time.h>
#include <unistd.h>

#define SEPARATOR ':'
#define SIZE_PWD 128

/**
 * Ajoute un compte au fichier
 * @param fp Pointeur vers le fichier
 * @param user Clef d'identification de l'utilisateur
 * @param password Mot de passe
 * @return Retourne le nombre de bits écrit dans le fichier, la valeur est négative si cela échoue
 */
int addAccount(FILE *fp, const char *user, const char *password);

/**
 * Récupère un mot de passe dans le fichier en cherchant la clef de l'utilisateur
 * @param fp Pointeur vers le fichier
 * @param user Clef d'identification de l'utilisateur
 * @return NULL si la clef n'est pas trouvé, le mot de passe sinon
 */
char *getPassword(FILE *fp, const char *user);

/**
 * Vérifie si le mot de passe chiffré est similaire au mot de passe rentré par l'utilisateur
 * @param encrypt Mot de passe chiffré
 * @param password Mot de passe rentré non chiffré
 * @return true si les mots de passe sont similaires, false sinon
 */
int isPassword(const char *encrypt, const char *password);

/**
 * Génére une clef de chiffrement
 * @return Un String correspont à la clef
 */
char *generateSalt();

#endif //ISI_TP1_DROITS_CHECK_PASS_H
