#include "check_pass.h"

int addAccount(FILE *fp, const char *user, const char *password) {
    return fprintf(fp, "%s%c%s\n", user, SEPARATOR, password);
}

char *getPassword(FILE *fp, const char *user) {
    char line[SIZE_PWD];
    int found = 0;

    while (found == 0 && fgets(line, SIZE_PWD, fp) != NULL) {
        found = startsWith(user, line);
    }

    if (found) {
        int indexSplit = indexOf(line, SEPARATOR);
        if (indexSplit != -1) {
            return substr(line, indexSplit + 1, strlen(line) - 1);
        }
    }

    return NULL;
}

int isPassword(const char *encrypt, const char *password) {
    char *passwordCrypt = crypt(password, encrypt);
    return strcmp(passwordCrypt, encrypt) == 0;
}

char *generateSalt() {
    char salt[] = "$1$........";
    unsigned long seed[2];
    const char *const seedchars = "./0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";

    seed[0] = time(NULL);
    seed[1] = getpid() ^ (seed[0] >> 14 & 0x30000);

    for (int i = 0; i < 8; i++) {
        salt[3 + i] = seedchars[(seed[i / 5] >> (i % 5) * 6) & 0x3f];
    }

    char *returnSalt = malloc(sizeof(salt));
    strcpy(returnSalt, salt);
    return returnSalt;
}