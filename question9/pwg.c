#include <stdlib.h>
#include "check_pass.h"
#include "string.h"
#include <string.h>

#define FILENAME_PWD "/home/admin/passwd"

/**
 * Récupére le uid de l'utilisateur osus forme de string
 * @return String avec le uid
 */
char *uidToStr() {
    uid_t uid;
    uid = getuid();
    char *uidstr = malloc(sizeof(char) * 10);
    sprintf(uidstr, "%d", uid);
    return uidstr;
}

int main() {
    FILE *fpPwd = fopen(FILENAME_PWD, "a+");
    if (!fpPwd) {
        perror("Impossible d'ouvrir le fichier\n");
        exit(EXIT_FAILURE);
    }

    char *uidstr = uidToStr();
    char *pwd = getPassword(fpPwd, uidstr);

    if (pwd != NULL) {
        int isSimilar = 0;
        while (!isSimilar) {
            char *lastPassword = getpass("Previous password:");
            isSimilar = isPassword(pwd, lastPassword);
        }
    }

    char *salt = generateSalt();
    char *newPwd = crypt(getpass("New password:"), salt);
    if (pwd != NULL) {
        if (addAccount(fpPwd, uidstr, newPwd) < 0) {
            perror("Unable to write in file");
        }
    } else if (addAccount(fpPwd, uidstr, newPwd) < 0) {
        perror("Unable to write in file");
    }

    fclose(fpPwd);
    exit(EXIT_SUCCESS);
}
