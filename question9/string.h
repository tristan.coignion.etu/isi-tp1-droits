#ifndef ISI_TP1_DROITS_STRING_H
#define ISI_TP1_DROITS_STRING_H

#include <string.h>
#include <stdlib.h>

/**
 * Trouve l'index dans le premier index d'un caractere dans un string
 * @param str String dans lequel le caractere sera cherché
 * @param value Caractere à chercher
 * @return L'index du char, -1 s'il n'est pas trouvé
 */
int indexOf(const char *str, char value);

/**
 * Récupére une sous-chaine comprise entre les deux index
 * @param src String source
 * @param m Index de début
 * @param n Index de fin
 * @return NULL si n est plus petit que m, le sous-chaine sinon
 */
char *substr(const char *src, int m, int n);

/**
 * Vérifie qu'un string commence par un autre string
 * @param prefix Prefix
 * @param source String source
 * @return true si le prefix est similaire au prefix du string source, false sinon
 */
int startsWith(const char *prefix, const char *source);

#endif //ISI_TP1_DROITS_STRING_H
